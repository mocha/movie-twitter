#!/usr/bin/python
# -*- coding: latin-1 -*-

from random import choice, randint
from twython import Twython
import os

consumer_key = os.environ.get("consumer_key")
consumer_secret = os.environ.get("consumer_secret")
access_token = os.environ.get("access_token")
access_token_secret = os.environ.get("access_token_secret")

twitter = Twython(
    consumer_key,
    consumer_secret,
    access_token,
    access_token_secret
)


def genTitle() :

    # title vars
    forebodingWord      = ["Doom", "Midnight", "Shadow", "Silent", "Hidden", "Twilight", "Zero", "Night", "Ghost", "Spectre", "Saber", "Final", "First", "Black", "Death", "Power", "Lamentation", "Sadness", "Singularity", "Despair", "Depression", "Lonely"]
    militaryUnit        = ["Squad", "Corps", "Team", "Unit", "Squadron", "Force", "Army", "Navy", "Battalion", "Brigade", "Regiment", "Division", "Platoon"]
    cardinalDirection   = ["Northern", "Eastern", "Southern", "Western", "Hidden", "Upward", "Downward", "Over", "Under", "Winter", "Summer", "Spring", "Fall"]
    powerAnimal         = ["Tiger", "Panther", "Puma", "Leopard", "Lynx", "Lion", "Eagle", "Hawk", "Falcon", "Raptor", "Squall", "Jackal", "Demon", "Ridgeback", "Sidewinder", "Viper", "Cobra", "Rattlesnake", "Cougar", "Lion", "Lemur", "Giraffe", "Buffalo", "Hippo", "Elephant", "Snake", "Frog", "Hyena", "Wolf"]
    orientation         = ["Down", "Rising", "Rises", "Falls", "Horizon", "Echo"]
    heroNoun            = ["Soul", "Souls", "Renegade", "Hunter", "Stalker", "Fighter", "Warrior", "Commando", "Knight", "Guard", "Gunner", "Gun", "Blade"]
    militaryAction      = ["Operation", "Code Name", "Covert", "Undercover", "Collateral", "Containment", "Mandatory"]
    aggressiveAction    = ["Revenge", "Attack", "Last", "Defense", "Destruction", "Explosion", "Detonation", "Panic", "Countdown", "Annihilation", "Apocalypse", "Explode", "Destroy", "Catastrophe", "Assassination"]
    superlative         = ["All Out", "Everyone", "Super", "Always", "Never", "Terminal"]
    colorWord           = ["Red", "Black", "Blue", "Green", "Steel", "Gold", "Invisible", "Dark", "Blue", "Purple", "Orange", "White", "Rainbow", "Midnight", "Onyx", "Sapphire", "Emerald", "Ruby", "Blinding", "Clear"]
    legendarium         = ["Legend", "Myth", "Story", "History", "Tale", "Book", "Chronicles"]
    
    formula_no = randint(0,8)
    
    if formula_no == 0:
        title = choice(forebodingWord) + " " + choice(militaryUnit)
    
    elif formula_no == 1:
        title = choice(cardinalDirection) + " " + choice(heroNoun)
    
    elif formula_no == 2:
        object1 = choice([choice(forebodingWord), choice(powerAnimal), choice(militaryAction)])
        object2 = choice([choice(orientation), choice(heroNoun), choice(militaryUnit)])
        title = object1 + " " + object2
    
    elif formula_no == 3:
        title = choice(colorWord) + " " + choice(powerAnimal)

    elif formula_no == 4:
        title = choice(powerAnimal) + " " + choice(orientation)

    elif formula_no == 5:
        title = choice(superlative) + " " + choice(aggressiveAction)
    
    elif formula_no == 6:
        title = choice(militaryAction) + " " + choice(forebodingWord)
        
    elif formula_no == 7:
        title = choice(legendarium) + " of the " + choice(colorWord) + " " + choice(powerAnimal)

    elif formula_no == 8:
        title = choice(forebodingWord)

    
    postfixes = [
        choice([" 1", " 2", " 3", " 4", " 5"]),
        choice([": Part One", ": Part Two", ": Part Three", ": Part Four", ": The Sequel"]), 
        choice([" II", " III", " IV", " V"]),
        choice([" Returns", " Strikes Back", " Rides Again", " Again", ": Redux", " Homecoming"]),
        choice([": The Final Battle", ": The Reckoning", ": A New Beginning", ": Origins", ": Retribution", ": Legacy",
        ": Annihilation", ": Overture", ": Endgame ", ": The Final Battle", ": Exodus"]),
        choice([": Miami", ": Singapore", ": Wild West", ": Chicago", ": London", ": Paris", ": Worldwide"]),
        choice([
            ": " + choice(colorWord) + " Symphony",
            ": " + choice(forebodingWord),
            ": " + choice(forebodingWord) + " " + choice(militaryUnit),
            ": " + choice(heroNoun)
        ])
    ]
    
    postfix_chance = 50
    if randint(0,100) < postfix_chance:
        title = title + choice(postfixes)
        
    return title



def genDate():
    return choice([
        choice(["Spring ", "Summer ", "Fall ", "Winter ", "Late ", "Early ", "Mid-"]) + str(randint(1975, 1995)),
        choice(["70s", "80s", "90s", "00s"]),
        choice(["Early ", "Mid-", "Late ", ""]) + choice(["70s", "80s", "90s", "00s"])
    ])


def genActor(solo=False):
    actors = ["Adam Baldwin", "Adrian Paul", "Adrienne Barbeau", "Adrienne King", "Al Cliver", "Alan Van Sprang", "Ali Larter", "Alice Cooper", "Ally Sheedy", "Andrew Divoff", "Andrew Robinson", "Andy Serkis", "Angela Bassett", "Angelina Jolie", "Angus Scrimm", "Anthony Perkins", "Antonio Banderas", "Antonio Fargas", "Armand Assante", "Arnold Schwarzenegger", "Arnold Vosloo", "Asia Argento", "Barbara Crampton", "Barbara Steele", "Bela Lugosi", "Bernie Casey", "Betsy Palmer", "Bill Moseley", "Bill Paxton", "Billy Bob Thornton", "Billy Drago", "Blu Mankuma", "Bo Hopkins", "Bolo Yeung", "Boris Karloff", "Brad Dourif", "Bradley Gregg", "Brandon Lee", "Brenda Bakke", "Brian Bosworth", "Brian Thompson", "Brigitte Nielsen", "Brion James", "Bruce Abbott", "Bruce Campbell", "Bruce Glover", "Bruce Lee", "Bruce Payne", "Bruce Willis", "Bubba Smith", "Burt Reynolds", "C.J. Graham", "Candy Clark", "Carl Weathers", "Cary-Hiroyuki Tagawa", "Casper Van Dien", "Charles Bronson", "Charles Cyphers", "Charles Napier", "Charles S. Dutton", "Charlie Sheen", "Chow Yun-Fat", "Chris Penn", "Chris Sarandon", "Chris Tucker", "Christian Bale", "Christopher Lambert", "Christopher Lee", "Chuck Norris", "Clancy Brown", "Clarence Williams III", "Cliff De Young", "Clint Eastwood", "Clint Howard", "Clint Walker", "Clive Owen", "Clu Gulager", "Corbin Bernsen", "Corey Feldman", "Corey Haim", "Courteney Cox", "Craig Fairbrass", "Craig Sheffer", "Craig Wasson", "Crispin Glover", "Cuba Gooding Jr.", "Cynthia Rothrock", "Damon Wayans", "Dan O'Herlihy", "Daniel Craig", "Daniel von Bargen", "Danielle Harris", "Danny Glover", "Danny Trejo", "Darren McGavin", "David Arquette", "David Carradine", "David Patrick Kelly", "David Warbeck", "David Warner", "Dean Cameron", "Dean Norris", "Dee Wallace", "Delroy Lindo", "Denis Leary", "Dennis Farina", "Dennis Hopper", "Denzel Washington", "Devon Sawa", "Dick Miller", "Dirk Blocker", "Dolph Lundgren", "Dominique Pinon", "Don Keith Opper", "Don Wilson", "Donald Pleasence", "Donald Sutherland", "Donnie Yen", "Doug Bradley", "Doug Jones", "Duane Davis", "Dwayne Johnson", "Ed Harris", "Elias Koteas", "Emile Hirsch", "Emilio Estevez", "Eric Roberts", "Ernest Borgnine", "Ernie Hudson", "Frank Zagarino", "Fred Ward", "Fred Williamson", "Gary Busey", "Gary Daniels", "Gene Hackman", "Geoffrey Lewis", "George 'Buck' Flower", "George Wendt", "Gerard Butler", "Gina Gershon", "Greg Grunberg", "Gregg Henry", "Gunnar Hansen", "Hal Holbrook", "Harrison Ford", "Harvey Keitel", "Heather Langenkamp", "Henry Da Silva", "Henry Rollins", "Hugh Jackman", "Hugh Quarshie", "Hugo Weaving", "Hulk Hogan", "Ian McCulloch", "Isaac Hayes", "Jack Noseworthy", "Jack Palance", "Jackie Chan", "James Hong", "James Karen", "James Le Gros", "James Remar", "James Tolkan", "James Wainwright", "Jamie Lee Curtis", "Jan-Michael Vincent", "Jason Mewes", "Jason Statham", "Jean Reno", "Jean-Claude Van Damme", "Jeff Fahey", "Jeff Goldblum", "Jeffrey Jones", "Jennifer Rubin", "Jeremy Sisto", "Jesse Ventura", "Jet Li", "Jim Brown", "Jim Kelly", "Joe Estevez", "Joe Mantegna", "John C. McGinley", "John Carradine", "John Glover", "John Leguizamo", "John Malkovich", "John Philbin", "John Rhys-Davies", "John Saxon", "John Spencer", "Joseph Pilato", "Joss Ackland", "Julian Sands", "Kane Hodder", "Karen Black", "Karl Urban", "Keanu Reeves", "Keith David", "Ken Foree", "Kevin Corrigan", "Kevin Costner", "Kevin J. O'Connor", "Kevin Zegers", "Kiefer Sutherland", "Kim Delaney", "Klaus Kinski", "Kurt Russell", "LL Cool J", "Lance Henriksen", "Laurence Fishburne", "Laurene Landon", "Laurie Holden", "Lee Van Cleef", "Lee de Broux", "Lewis Collins", "Liam Neeson", "Linda Blair", "Linda Hamilton", "Lorenzo Lamas", "Lou Diamond Phillips", "Lou Ferrigno", "Lyle Alzado", "M. Emmet Walsh", "Maila Nurmi", "Mako", "Malcolm McDowell", "Marc Singer", "Mario Van Peebles", "Marjoe Gortner", "Mark Boone Junior", "Mark Dacascos", "Mark Hamill", "Mark Rolston", "Marshall Bell", "Martin Kove", "Matt Frewer", "Meg Foster", "Mel Gibson", "Melinda Clarke", "Melvin Van Peebles", "Michael Beck", "Michael Berryman", "Michael Biehn", "Michael Caine", "Michael Dudikoff", "Michael Eklund", "Michael Gross", "Michael Ironside", "Michael Jai White", "Michael Madsen", "Michael Massee", "Michael Moriarty", "Michael Paré", "Michael Rooker", "Michael Wincott", "Michelle Rodriguez", "Michelle Yeoh", "Mickey Rourke", "Miguel A. Núñez Jr.", "Miguel Ferrer", "Milla Jovovich", "Mitch Pileggi", "Mr. T", "Nancy Allen", "Natasha Henstridge", "Nathan Fillion", "Nicolas Cage", "Norman Reedus", "P.J. Soles", "Pam Grier", "Patricia Tallman", "Patrick Kilpatrick", "Patrick Macnee", "Patrick Swayze", "Paul Bettany", "Paul Winfield", "Peter Coyote", "Peter Cushing", "Peter Fonda", "Peter Greene", "Peter Jason", "Peter Weller", "Pierce Brosnan", "Piper Laurie", "Powers Boothe", "R.G. Armstrong", "Randall 'Tex' Cobb", "Ray Liotta", "Ray Winstone", "Ray Wise", "Raymond Cruz", "Reb Brown", "Reggie Bannister", "Richard Brooks", "Richard Chamberlain", "Richard Chaves", "Richard Kiel", "Richard Lynch", "Richard Moll", "Richard Roundtree", "Robert Carradine", "Robert Davi", "Robert Englund", "Robert Forster", "Robert Ginty", "Robert John Burke", "Robert Patrick", "Robert Vaughn", "Robert Z'Dar", "Robin Shou", "Roddy McDowall", "Ron Perlman", "Roy Chiao", "Roy Frumkes", "Rutger Hauer", "Sam Bottoms", "Sam Neill", "Samuel L. Jackson", "Sandra Bullock", "Sarah Douglas", "Sasha Mitchell", "Scott Adkins", "Sean Bean", "Sean Connery", "Sean Patrick Flanery", "Sean Pertwee", "Sean Young", "Shane Black", "Shannon Tweed", "Sharon Stone", "Sherilyn Fenn", "Sheryl Lee", "Sid Haig", "Sigourney Weaver", "Sonny Landham", "Stacy Keach", "Stephen Dorff", "Stephen Geoffreys", "Stephen Macht", "Stephen Root", "Steve Austin", "Steve Guttenberg", "Steve James", "Steve McQueen", "Steve Railsback", "Steven Seagal", "Sven-Ole Thorsen", "Sybil Danning", "Sylvester Stallone", "Ted Raimi", "Terry Crews", "Thom Mathews", "Thomas Jane", "Thomas Rosales Jr.", "Tim Daly", "Tim Matheson", "Tim Thomerson", "Timothy Olyphant", "Tobin Bell", "Tom Atkins", "Tom Berenger", "Tom Cruise", "Tom Lister Jr.", "Tom Savini", "Tom Sizemore", "Tom Towles", "Tommy Lee Jones", "Tony Burton", "Tony Todd", "Tracey Walter", "Traci Lords", "Treat Williams", "Trevor Goddard", "Val Kilmer", "Vanity", "Vernon Wells", "Victor Wong", "Viggo Mortensen", "Vin Diesel", "Vincent Price", "Ving Rhames", "Vinnie Jones", "Virginia Madsen", "Wes Studi", "Wesley Snipes", "Will Smith", "Willem Dafoe", "William Hickey", "William Katt", "William Sadler", "William Sanderson", "William Smith", "Wings Hauser", "Xander Berkeley", "Yancy Butler", "Yaphet Kotto", "Zach Galligan", "Zakes Mokae"
    ]
    
    double_chance = 35
    if (randint(0,100) > double_chance) or solo==True:
        return choice(actors)
    else:
        return choice(actors) + " and " + choice(actors)


def genAward():
    return choice(["Best Picture", "Best Supporting Actor", "Best Cinematography", "Best Actor", "Best International Feature Film", "Best Film Editing", "Best Actress", "Best Original Song", "Best Documentary Feature", "Best Sound Mixing", "Best Visual Effects", "Best Director", "Best Supporting Actress", "Best Original Screenplay", "Best Adapted Screenplay", "Best Costume Design", "Best Production Design", "Best Dance Direction", "Best Makeup and Hairstyling", "Best Story", "Best Assistant Director", "Best Title Writing", "Best Original Song Score", "Best Engineering Effects", "Best Dramatic Picture Direction", "Best Comedy Picture Direction", "Best Writing" ])


def genYear():
    return str(randint(1975, 1995))

def genRecord():
    
    nouns = ["lighting", "gasoline", "guns", "prop guns", "fake plants", "actual bullets", "vehicles", "prop food", "trains", "prop houses", "trees", "costumes", "foley effects", "camera film"]
    people = ["actors", "grips", "cameramen", "makeup staff", "crafts staff", "lawyers", "marketing staff", "effects engineers"]
    people_action = ["sued", "injured", "fired", "replaced", "who quit", "who demanded royalties"]
    return choice([
        "budget spent on " + choice(nouns),
        choice(nouns) + " destroyed",
        choice(nouns) + " used",
        choice(people) + " " + choice(people_action)
    ])


def genGenre():
    return choice(["action", "adventure", "animation", "biographic", "comedy", "crime", "documentary", "drama", "family", "fantasy", "historical", "horror", "musical", "mystery", "romance", "sci-fi", "sports", "thriller", "war", "western"])


def genTweet():
    return choice([
        "Tonight, check out the forgotten " + genDate() + " classic, \"" + genTitle() + "\" starring " + genActor() + ".",
        "Dive in to " + genActor(solo=True) + "'s early acting -- start with " + genTitle() + " or " + genTitle(),
        "Watch the " + genYear() + " " + genAward() + " nominee: " + genTitle(),
        "Hot take: " + genActor(solo=True) + " hit the peak of their career in " + genTitle(),
        "The pinnacle of the " + genGenre() + "/" + genGenre() + " genre was " + genActor(solo=True) + " and " + genActor(solo=True) + " in " + genTitle(),
        "Today's film fact: \"" + genTitle() + "\" holds the record for most " + genRecord() + " in a single film.",
        "Looking to get in to action-" + genGenre() + " films? Check out the " + genDate() + " classic, \"" + genTitle() + "\"",
        "The cinematography of " + genTitle() + " is one of the best examples of " + genGenre() + " movies as high-art from " + genDate(),
        "A stunning example of a directorial debut -- " + genActor(solo=True) + "'s first film, " + genTitle(),
        "One of the best trios in film: " + genActor(solo=True) + ", " + genActor(solo=True) + ", and " + genActor(solo=True) + ".",
        "Have you seen \"" + genTitle() + "\"? If so, a great follow up if you like " + genActor(solo=True) + " is \"" + genTitle() + "\"",
        "Know your " + genAward() + " winners -- " + genTitle() + ", winner in " + genYear(),
        "\"" + genTitle() + "\" is probably the best " + genGenre() + "/" + genGenre() + " of all time, and \"" + genTitle() + "\" is a solid runner-up.",
        "For a weekend binge, consider \"" + genTitle() + "\" and the " + genActor(solo=True) + "-directed spinoff, \"" + genTitle() + "\"",
        "Love " + genActor() + "? Check out their work in " + genTitle()
    ])
    

message = genTweet()
twitter.update_status(status=message)
